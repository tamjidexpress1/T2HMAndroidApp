package com.t2.t2hm;

/**
 * Created by Tamjid on 8/18/2017.
 */

import android.app.Activity;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.SimpleExpandableListAdapter;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import javax.net.ssl.HttpsURLConnection;

//import static com.t2.t2hm.BluetoothLeService.UUID_HEART_RATE_MEASUREMENT;

/**
 * For a given BLE device, this Activity provides the user interface to connect, display data,
 * and display GATT services and characteristics supported by the device.  The Activity
 * communicates with {@code BluetoothLeService}, which in turn interacts with the
 * Bluetooth LE API.
 */
public class DeviceControlActivity extends Activity {
    private final static String TAG = "devicecontrol";
    private final static  double TempCoefficientOfADC=3300/31180;
    public static final String EXTRAS_DEVICE_NAME = "DEVICE_NAME";
    public static final String EXTRAS_DEVICE_ADDRESS = "DEVICE_ADDRESS";

    private TextView mConnectionState;
    private EditText uNameDataField;
    private TextView mTempDataField;
    private TextView mHRDataField;
    Button button;
    private String mDeviceName;
    private String mDeviceAddress;
    private ExpandableListView mGattServicesList;
    private BluetoothLeService mBluetoothLeService;
    private ArrayList<ArrayList<BluetoothGattCharacteristic>> mGattCharacteristics =
            new ArrayList<ArrayList<BluetoothGattCharacteristic>>();
    private boolean mConnected = false;
    private BluetoothGattCharacteristic mNotifyCharacteristic;

    private final String LIST_NAME = "NAME";
    private final String LIST_UUID = "UUID";
//characteristics uuid fields
    public final static UUID UUID_TEMP_MEASUREMENT =
            UUID.fromString(SampleGattAttributes.TEMP_MEASUREMENT);
    public final static UUID UUID_HEART_RATE_MEASUREMENT =
            UUID.fromString(SampleGattAttributes.HEART_RATE_MEASUREMENT);
    public final static UUID UUID_MYHR_MEASUREMENT =
            UUID.fromString(SampleGattAttributes.MYHR_MEASUREMENT);
    //data sending urls
    public String tempUri="https://siham009.pythonanywhere.com/temp_datas/";
    public String hRateUri="https://siham009.pythonanywhere.com/pulse_datas/";
    // Code to manage Service lifecycle.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            mBluetoothLeService = ((BluetoothLeService.LocalBinder) service).getService();
            if (!mBluetoothLeService.initialize()) {
                Log.e(TAG, "Unable to initialize Bluetooth");
                finish();
            }
            // Automatically connects to the device upon successful start-up initialization.
            mBluetoothLeService.connect(mDeviceAddress);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBluetoothLeService = null;
        }
    };

    // Handles various events fired by the Service.
    // ACTION_GATT_CONNECTED: connected to a GATT server.
    // ACTION_GATT_DISCONNECTED: disconnected from a GATT server.
    // ACTION_GATT_SERVICES_DISCOVERED: discovered GATT services.
    // ACTION_DATA_AVAILABLE: received data from the device.  This can be a result of read
    //                        or notification operations.
    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (BluetoothLeService.ACTION_GATT_CONNECTED.equals(action)) {
                mConnected = true;
                updateConnectionState(R.string.connected);
                invalidateOptionsMenu();
            } else if (BluetoothLeService.ACTION_GATT_DISCONNECTED.equals(action)) {
                mConnected = false;
                updateConnectionState(R.string.disconnected);
                invalidateOptionsMenu();
                clearUI();
            } else if (BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {
                // Show all the supported services and characteristics on the user interface.
                //displayGattServices(mBluetoothLeService.getSupportedGattServices());
                enableNotificationOnCustomCharacteristics(mBluetoothLeService.getSupportedGattServices());
            } else if (BluetoothLeService.ACTION_DATA_AVAILABLE.equals(action)) {
                Log.d(TAG,"code entered in broadcastreciever action data available");
                displayData(intent.getStringExtra(BluetoothLeService.EXTRA_DATA));

            }
        }
    };

    // If a given GATT characteristic is selected, check for supported features.  This sample
    // demonstrates 'Read' and 'Notify' features.  See
    // http://d.android.com/reference/android/bluetooth/BluetoothGatt.html for the complete
    // list of supported characteristic features.
    private final ExpandableListView.OnChildClickListener servicesListClickListner =
            new ExpandableListView.OnChildClickListener() {
                @Override
                public boolean onChildClick(ExpandableListView parent, View v, int groupPosition,
                                            int childPosition, long id) {
                    if (mGattCharacteristics != null) {
                        final BluetoothGattCharacteristic characteristic =
                                mGattCharacteristics.get(groupPosition).get(childPosition);
                        final int charaProp = characteristic.getProperties();
                        if ((charaProp | BluetoothGattCharacteristic.PROPERTY_READ) > 0) {
                            // If there is an active notification on a characteristic, clear
                            // it first so it doesn't update the data field on the user interface.
                            if (mNotifyCharacteristic != null) {
                                mBluetoothLeService.setCharacteristicNotification(
                                        mNotifyCharacteristic, false);
                                mNotifyCharacteristic = null;
                            }
                            mBluetoothLeService.readCharacteristic(characteristic);
                        }
                        if ((charaProp | BluetoothGattCharacteristic.PROPERTY_NOTIFY) > 0) {
                            mNotifyCharacteristic = characteristic;
                            mBluetoothLeService.setCharacteristicNotification(
                                    characteristic, true);
                        }
                        return true;
                    }
                    return false;
                }
            };

    private void clearUI() {
        mGattServicesList.setAdapter((SimpleExpandableListAdapter) null);
        mTempDataField.setText(R.string.no_data);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gatt_services_characteristics);

        final Intent intent = getIntent();
        mDeviceName = intent.getStringExtra(EXTRAS_DEVICE_NAME);
        mDeviceAddress = intent.getStringExtra(EXTRAS_DEVICE_ADDRESS);

        // Sets up UI references.
        ((TextView) findViewById(R.id.device_address)).setText(mDeviceAddress);
        mGattServicesList = (ExpandableListView) findViewById(R.id.gatt_services_list);
        mGattServicesList.setOnChildClickListener(servicesListClickListner);
        mConnectionState = (TextView) findViewById(R.id.connection_state);
        mTempDataField = (TextView) findViewById(R.id.temp_value);
        mHRDataField = (TextView) findViewById(R.id.hr_value);
        uNameDataField= (EditText) findViewById(R.id.user_name);
        button = (Button) findViewById(R.id.button);
        getActionBar().setTitle(mDeviceName);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        Intent gattServiceIntent = new Intent(this, BluetoothLeService.class);
        bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
        if (mBluetoothLeService != null) {
            final boolean result = mBluetoothLeService.connect(mDeviceAddress);
            Log.d(TAG, "Connect request result=" + result);
        }
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button
               String username= uNameDataField.getText().toString();
                String tempdata= mTempDataField.getText().toString();
                sendTempDataTask sendTask=new sendTempDataTask();
                sendTask.execute(username,tempdata);

            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mGattUpdateReceiver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindService(mServiceConnection);
        mBluetoothLeService = null;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.gatt_services, menu);
        if (mConnected) {
            menu.findItem(R.id.menu_connect).setVisible(false);
            menu.findItem(R.id.menu_disconnect).setVisible(true);
        } else {
            menu.findItem(R.id.menu_connect).setVisible(true);
            menu.findItem(R.id.menu_disconnect).setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.menu_connect:
                mBluetoothLeService.connect(mDeviceAddress);
                return true;
            case R.id.menu_disconnect:
                mBluetoothLeService.disconnect();
                return true;
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void updateConnectionState(final int resourceId) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mConnectionState.setText(resourceId);
            }
        });
    }

    private void displayData(String data) {
        String str = "";
        str=data;
        char[] charArray = str.toCharArray();
        if (charArray[0] == 'T') {
            char[] tCharArray= Arrays.copyOfRange(charArray, 1, charArray.length-1);;
            Log.d(TAG,new String(tCharArray));
            String fHexStr=new String(tCharArray);
            fHexStr=fHexStr.replaceAll("\\W","");
            int tempADC = Integer.parseInt(fHexStr, 16 );
            Log.d(TAG,String.format("tempADC %d",tempADC));
            double plainTemp=.105837075*tempADC;
            String stringTemp=fmt(plainTemp);
            Log.d(TAG,String.format(plainTemp+"  plainTemp"));
            mTempDataField.setText(String.format(stringTemp));
           // sendTempDataTask sendTask=new sendTempDataTask();
            //sendTask.execute(plainTemp);

        }
        if (charArray[0] == 'H') {
            char[] tCharArray= Arrays.copyOfRange(charArray, 1, charArray.length-1);;
            Log.d(TAG,new String(tCharArray));
            String fHexStr=new String(tCharArray);
            fHexStr=fHexStr.replaceAll("\\W","");
            int hRateADC = Integer.parseInt(fHexStr, 16 );
            Log.d(TAG,String.format("hRateADC %d",hRateADC));
            double plainhRate=hRateADC;
            String stringhRate=fmt(plainhRate);
            Log.d(TAG,String.format(plainhRate+"  plainTemp"));
            mHRDataField.setText(String.format(stringhRate+"BPM"));
            sendHRDataTask sendHR=new sendHRDataTask();
            sendHR.execute(plainhRate);
        }
    }


    //Formats the float final sensor value into printable string
    public static String fmt(double d)
    {
        if(d == (long) d)
            return String.format("%d",(long)d);
        else
            return String.format("%s",d);
    }
    private class sendTempDataTask extends AsyncTask<String,String,Integer>{
//{"id":7,"name":"patient_04","temperature":35.0}
//BufferedReader reader = null;
        HttpsURLConnection https;
        int status = 0;
        @Override
        protected Integer doInBackground(String... params) {
            JSONObject jsonObject=new JSONObject();

            //Log.d(TAG,"code in doinbackground");
            try {

                jsonObject.put("name",params[0]);
                jsonObject.put("temperature",Double.parseDouble(params[1]));
                String str= new String(jsonObject.toString());
                Log.d(TAG,"json data sent "+str);
                URL url=new URL(tempUri);
                https=(HttpsURLConnection) url.openConnection();
              //set methods
               https.setRequestMethod("POST");
                https.setDoOutput(true);
                //https.setDoInput(true);
                https.setRequestProperty("Content-Type", "application/json");


                if(https==null)
                {Log.d(TAG,"urlconnection returns null");}
                else{ {Log.d(TAG,"urlconnection is not null");}}
                OutputStreamWriter writer=new OutputStreamWriter(https.getOutputStream());
                if(writer==null){Log.d(TAG,"outputstream is null");}
                writer.write(str);
                writer.flush();
                Log.d(TAG,"data flushed succesfully");
                status = https.getResponseCode();
                /*if (status==  201) {
                    InputStream inputStream = https.getInputStream();
                    StringBuffer buffer = new StringBuffer();
                    if (inputStream == null) {
                        // Nothing to do.
                        return null;
                    }
                    reader = new BufferedReader(new InputStreamReader(inputStream));

                    String line;
                    while ((line = reader.readLine()) != null) {
                        // Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
                        // But it does make debugging a *lot* easier if you print out the completed
                        // buffer for debugging.
                        buffer.append(line + "\n");
                    }
                    Log.d(TAG,"return response"+line);
                   }*/

                    Log.d(TAG, "Status : " + status);
            } catch (JSONException e) {
                e.printStackTrace();
                Log.d(TAG,"json exception occured");
            } catch (IOException e) {
                e.printStackTrace();
                Log.d(TAG,"IO exception occured");
            }finally {
                if(https!=null)
                {https.disconnect();}
            }
            return status;
        }
    }
    private class sendHRDataTask extends AsyncTask<Double,String,Integer>{
        HttpsURLConnection https;
        int status = 0;
        @Override
        protected Integer doInBackground(Double... params) {
            JSONObject jsonObject=new JSONObject();

            try {
                jsonObject.put("name","tomal");
                jsonObject.put("hRate",params[0]);
                String str= new String(jsonObject.toString());
                Log.d(TAG,"json data sent "+str);
                URL url=new URL(hRateUri);
                https=(HttpsURLConnection) url.openConnection();
                //set methods
                https.setRequestMethod("POST");
                https.setDoOutput(true);
                //https.setDoInput(true);
                https.setRequestProperty("Content-Type", "application/json");

                if(https==null)
                {Log.d(TAG,"urlconnection returns null");}
                else{ {Log.d(TAG,"urlconnection is not null");}}
                OutputStreamWriter writer=new OutputStreamWriter(https.getOutputStream());
                if(writer==null){Log.d(TAG,"outputstream is null");}
                writer.write(str);
                writer.flush();
                Log.d(TAG,"data flushed succesfully");
                status = https.getResponseCode();

                Log.d(TAG, "Status : " + status);
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if(https!=null)
                {https.disconnect();}
            }
            return status;
        }
    }

    private void enableNotificationOnCustomCharacteristics(List<BluetoothGattService> gattServices){
        boolean dEna = false;
        for (BluetoothGattService gattService : gattServices){
            List<BluetoothGattCharacteristic> gattCharacteristics =
                    gattService.getCharacteristics();
            for (BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics) {
                if (UUID_TEMP_MEASUREMENT.equals(gattCharacteristic.getUuid())) {
                    mBluetoothLeService.setCharacteristicNotification(gattCharacteristic,true);
                    Log.d(TAG,"Notification enabled on temperature char");
                    dEna=true;
                }
            }
        }
        if(dEna==false)
        {Log.d(TAG,"temperature character was not found");}
    }

    // Demonstrates how to iterate through the supported GATT Services/Characteristics.
    // In this sample, we populate the data structure that is bound to the ExpandableListView
    // on the UI.
    private void displayGattServices(List<BluetoothGattService> gattServices) {
        if (gattServices == null) return;
        String uuid = null;
        String unknownServiceString = getResources().getString(R.string.unknown_service);
        String unknownCharaString = getResources().getString(R.string.unknown_characteristic);
        ArrayList<HashMap<String, String>> gattServiceData = new ArrayList<HashMap<String, String>>();
        ArrayList<ArrayList<HashMap<String, String>>> gattCharacteristicData
                = new ArrayList<ArrayList<HashMap<String, String>>>();
        mGattCharacteristics = new ArrayList<ArrayList<BluetoothGattCharacteristic>>();

        // Loops through available GATT Services.
        for (BluetoothGattService gattService : gattServices) {
            HashMap<String, String> currentServiceData = new HashMap<String, String>();
            uuid = gattService.getUuid().toString();
            currentServiceData.put(
                    LIST_NAME, SampleGattAttributes.lookup(uuid, unknownServiceString));
            currentServiceData.put(LIST_UUID, uuid);
            gattServiceData.add(currentServiceData);

            ArrayList<HashMap<String, String>> gattCharacteristicGroupData =
                    new ArrayList<HashMap<String, String>>();
            List<BluetoothGattCharacteristic> gattCharacteristics =
                    gattService.getCharacteristics();
            ArrayList<BluetoothGattCharacteristic> charas =
                    new ArrayList<BluetoothGattCharacteristic>();

            // Loops through available Characteristics.
            for (BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics) {
                charas.add(gattCharacteristic);
                HashMap<String, String> currentCharaData = new HashMap<String, String>();
                uuid = gattCharacteristic.getUuid().toString();
                currentCharaData.put(
                        LIST_NAME, SampleGattAttributes.lookup(uuid, unknownCharaString));
                currentCharaData.put(LIST_UUID, uuid);
                gattCharacteristicGroupData.add(currentCharaData);
            }
            mGattCharacteristics.add(charas);
            gattCharacteristicData.add(gattCharacteristicGroupData);
        }

        SimpleExpandableListAdapter gattServiceAdapter = new SimpleExpandableListAdapter(
                this,
                gattServiceData,
                android.R.layout.simple_expandable_list_item_2,
                new String[] {LIST_NAME, LIST_UUID},
                new int[] { android.R.id.text1, android.R.id.text2 },
                gattCharacteristicData,
                android.R.layout.simple_expandable_list_item_2,
                new String[] {LIST_NAME, LIST_UUID},
                new int[] { android.R.id.text1, android.R.id.text2 }
        );
        mGattServicesList.setAdapter(gattServiceAdapter);
    }

    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(BluetoothLeService.ACTION_DATA_AVAILABLE);
        return intentFilter;
    }
}
